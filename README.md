# EnvAdapt-CRO-SL for MATLAB

## Algorithm name
Environmentally Adaptive Coral Reefs Optimization with Substrate Layers (EnvAdapt-CRO-SL).

\[v.1.0\]:  *Thanks to the [Department of Automatic](https://www.uah.es/en/conoce-la-uah/campus-centros-y-departamentos/departamentos/Automatica/) of the [University of Alcalá](https://www.uah.es/en/) (2023).*

## Description
This GitLab project goal is the improvement of the standard CRO-SL algorithm adding new methods to its basic structure defined [here](https://ieeexplore.ieee.org/document/7744242). Ocean reefs are dynamics ecosystems subjected to the continous interaction of the different species which inhabit it and some random external inputs that it modifies the environment of the reef.

## Methodology
'Environmentally Adaptive' methods present a way to dynamize the behaviour of different metaheuristic algorithms through the addition of new 'environment operators'. This type of operators simulate the external environment pressure on the principal frameworks of the algorithms, changing some of the intrinsical values of it and improving the vein to break long solution convergence periods.

### :information_source: Classical CRO-SL operators:

- **Broadcast Spawning (Internal Sexual Reproduction):** At step k, pairs are selected from the group of broadcast spawner corals. These pairs undergo sexual crossover to create coral larvae, which are then released into the water. It's crucial to note that once two corals are chosen as parents for a larva, they are ineligible for selection again during step k; in essence, a pair of corals serves as parents only once per step.

- **Brooding (External Sexual Reproduction):** During each step k of the reef formation phase in the CRO algorithm, the proportion of corals reproducing through brooding is represented by f_b=1−F_b. Brooding involves creating a coral larva through a random mutation of the brooding-reproductive coral, with self-fertilization accounted for in hermaphrodite corals. Subsequently, the generated larva is released into the water similar to those produced by the broadcast spawning method.

- **Budding (Asexual Reproduction):** The entire collection of current corals in the reef is organized based on their fitness levels. From this arrangement, a portion of corals replicates and attempts to establish itself in another section of the reef using the larvae settlement procedure.

- **Larvae Settlement:** Generated larvae attempt to establish themselves in the reef through a two-step process. Each larva randomly tries to settle in a specific position of the reef. If the chosen position is unoccupied (free space in the reef), the coral grows there. However, if another coral already occupies the designated square, the new larva will settle only if its fitness function surpasses that of the existing coral (tournament).

- **Depredation:** Corals have the potential to perish during the reef formation phase. After the reproduction and settlement steps, a limited number of corals within the reef may undergo depredation, freeing up space for the subsequent generation of corals.

- **Substrates:** New functions integrated into the algorithm framework establish a dynamic assignment of operators by dividing the coral population into different layers, with each layer subjected to a specific substrate operator.

### :new: Specific environment operators applied to CRO-SL
In order to improve the performance of the CRO-SL algorithm, three new operators have been added to the framework of the classical algorithm structure:

- **Algae:** Introducing a novel structural element representing the encroachment of algae in certain reef zones. This new entity occupies specific positions within the Coral Reefs, stimulating competitive interactions and facilitating the replacement of corals instead of their natural colonization in vacant areas of the reef.

- **Acidification:** This process simulates chemical alterations in water chemistry resulting from increased CO2 levels, leading to a decline in pH (standard ocean water pH typically ranges from 8.1 to 8.2). A gradual devaluation of coral fitness over iterations characterizes the acidification trend, adversely affecting established corals within the reef. A diminishing maximum fitness threshold is enforced, disrupting coral convergence.

- **Adaptive Substrate Selection:** The incorporation of new operators into the coral population dynamics enables the implementation of a dynamic and adaptive approach to substrate selection for coral growth. Substrates are categorized into three distinct types: *Crossover*, *Mutation*, and *Swarm*. The probabilities associated with each substrate type are dynamically adjusted throughout iterations based on coral population convergence and their previous efficacy in generating optimal larvae.


#### :up: Available substrates
Some sustrates have been developed in order to be used in the EnvAdapt-CRO-SL framework:

- **Crossover substrates (1, 4):** 1Px, 2Px, MPx, BLX$\alpha\beta$.

- **Mutation substrates (2, 3):** GM, RndM, PermM.

- **Swarm substrates (3, 1):** PSO.

- **Physical-based substrates (4, 1):** SA.

- **Human-based substrates (5, 1):** TS.

## Guides and examples of use
Not available yet. *Work in progress...*


## Authors and acknowledgment
- Code main author: [Antonio J. Romero Barrera](https://scholar.google.es/citations?user=2HE_0PEAAAAJ).

- CRO Intellectual author: [Sancho Salcedo Sanz](https://scholar.google.es/citations?user=_X03BfUAAAAJ).

### Code citation ([style](https://uark.libguides.com/CSCE/CitingCode))
```Matlab
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
```


Antonio J. Romero Barrera. *PhD student*.


## License
[GNU General Public License v.3.0](https://gitlab.com/envadaptcrosl/matlab/-/blob/main/LICENSE)

## Project status
Work in progress...

- [X] Main algorithm structure.
- [X] Development of substrates.
- [X] Testing and comparison with other algorithms.
- [ ] Tutorials & manuals.

## Contact & info
- e-mail: antoniojose.romero@uah.es
- [Personal web page](https://sites.google.com/view/antoniojromero/)
- [Coral Reefs Optimization articles](http://agamenon.tsc.uah.es/Personales/sancho/CRO.html)

## Funding

\[v.1.0\]: Wi-DAI project (CM/JIN/2021–004) of the Community of Madrid and University of Alcalá.
