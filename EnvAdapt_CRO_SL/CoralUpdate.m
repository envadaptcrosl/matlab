%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

function [coral] = coralUpdate(env, coral, algae)

    %% Depredation procedure
    %%%---------------------
    if env.oceanAlgae == true
        coral.idx =  find( sum( isnan(coral.population), 2 ) == 0 ); % index of holded positions of the coral
        population = coral.population( coral.idx ); % remove algae and free positions
        [uniqueCorals, idxUnique, idxRep] = unique(round(coral.population,6), 'rows'); % get unique corals of the reef
        deathCandidates = setdiff(1:env.reefSize, idxUnique); % identify repeated corals
        r = rand( length(deathCandidates), 1 ); % random values for selection
        deathCorals = r < coral.deathRate; % random selection between repeated corals
        deathIdx = deathCandidates( find(deathCorals) ); % index of the death corals
        coral.population(deathIdx, :) = NaN * zeros( length(deathIdx), env.nVars ); % free the position of the reef
        coral.fitness(env.currentIter, deathIdx) = env.emptyPosVal; % update fitness value of empty position
        coral.idx =  find( sum( isnan(coral.population), 2 ) == 0 ); % index of holded positions of the coral
    else
        [env, coral] = Depredation(env, coral, algae);
        if (exist(env.subsSwarm))
            % Update reference points for Swarm substrates
            if ( sum( env.subsSwarm  == "PSO") > 0 ) %  update PSO parameters
                env = UpdateParamPSO(env, coral);
            end
        end
    end
    % env.Fb = length(unique(coral.population, 'rows'))/env.reefSize;
    % env.fb = 1 - Fb;
end