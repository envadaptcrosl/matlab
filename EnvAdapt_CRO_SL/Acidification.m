%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 


function [env, coral] = Acidification(env, coral)
    %% Non-influenced acidification by algae
    if env.trend == "max"
        if (env.oceanAlgae ==  true)
            env.acid = 1 - env.lambda * abs(env.pHmax-env.pH)/env.pHmax;
            coral.fitness(env.currentIter, :) = coral.fitness(env.currentIter-1, :).*env.acid; % update coral fitness
        else
            env.acid = min(env.acidBdry) + ( max(env.acidBdry) ...
                - min(env.acidBdry) ) .* rand(env.reefSize, 1); % update acidification value
            coral.fitness(env.currentIter, :) = coral.fitness(env.currentIter-1, :).*env.acid.'; % update coral fitness
        end
    else
        if (env.oceanAlgae ==  true)
            env.acid = 1 + env.lambda * abs(env.pHmax-env.pH)/env.pHmax;
            coral.fitness(env.currentIter, :) = coral.fitness(env.currentIter-1, :).*env.acid; % update coral fitness
        else
            env.acid = min(env.acidBdry) + ( max(env.acidBdry) ...
                - min(env.acidBdry) ) .* rand(env.reefSize, 1); % update acidification value
            coral.fitness(env.currentIter, :) = coral.fitness(env.currentIter-1, :).*(1+1-env.acid).'; % update coral fitness
        end

    end
end
