%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 


function larvae = SubsPhysical(env, larvae, population)
    idxCoral = find( sum( isnan(population), 2 ) == 0 ); % get index of coral positions in the reef
    population = population(idxCoral, :); % remove non coral positions (free or algae sites)
    if (sum(env.probSubsPhys) > 0) && (sum(env.probSubsTypePhys) > 0) && (size(population, 1) > 0)
        internalAssign = randsrc(size(population, 1), 1, [env.subsPhysIdx.'; round((env.probSubsPhys./env.probSubsTypePhys),15).']);
        %% Calculate new larvae from an specific substrate
        for k = 1:1:length(env.subsPhysIdx)
            fun = str2func( env.subsTable.subs( env.subsPhysIdx(k) ) ); % set function name
            population( find(internalAssign == env.subsPhysIdx(k)), : ) = ...
                fun( env, population( find( internalAssign == env.subsPhysIdx(k) ), :) );
            assignment( find(internalAssign == env.subsPhysIdx(k) ) ) = env.subsPhysIdx(k);
        end
        larvae.population = [larvae.population; population];
        larvae.assignment = [larvae.assignment assignment];
    end
end
