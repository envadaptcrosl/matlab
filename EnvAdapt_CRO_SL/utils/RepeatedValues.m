%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 


%% Function that check for repeated values, replacing it for random values

function [larvae] = RepeatedValues(env, larvae)
    for k = 1:size(larvae, 1)
        while length(unique(population(k, :))) < env.nVars
            [~, idxNonRep] = unique(larvae.population(k, :));
            idxRep = setdiff(1:env.reefSize, idxNonRep);
            for m = 1 : length(idxRep)
                larvae.population(k, idxRep(m)) = randi([env.varBdry(idxRep(m), 1), env.varBrdy(idxRep(m), 2)]);
            end
        end
    end
end
 