%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

function acidValues = CalcAcidification(env, algae)
    algaeDist = AlgaeDistance(algae);
    algaePos = find(algae.mask == 1);
    coralPos = find(algae.mask == 0);
    nAlgae = length(algaePos);
    for i=1:1:length(coralPos)
        x = (env.reefSize - algaeDist(1,:)))/env.reefSize; % values adimensionalization
        s(i)=(2*sigmoid(x)-1)*(nAlgae/env.reefSize); % calculate sigmoidal range values
        H(i) = env.H0 - env.incrH*s(i); % calculate H value
        acidValues(i) = -log10(H(i)) % calculate final acid values for next iteration
    end
end