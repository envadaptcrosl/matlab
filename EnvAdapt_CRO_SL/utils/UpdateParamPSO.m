%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

function [env] = UpdateParamPSO(env, coral)
    % copy the best coral results in the 'env' struct / swarm substrates references
    env.gBest(env.currentIter, :) = coral.bestCoral(env.currentIter, :);
    env.gBestFit(env.currentIter) = coral.bestFitness(env.currentIter);
    for n = 2:1:env.reefSize % for each position of the reef...
        if ( sum(isnan(coral.population), 2) == 0 ) % if position is held
            if ( coral.fitness(env.currentIter-1, n) > coral.fitness(env.currentIter, n) ) % if new fitness is better than previous iteration
                env.xBestFit(n) = coral.fitness(env.currentIter, n); % best fitness for that reef position
                env.xBest(n, :) = coral.population(n, :);
            end
        end
    end
end