%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 


function [larvae] = NonRepeatedVals(env, larvae)

    for k=1:1:size(larvae.population(larvae.idx), 1)
    
        [uniqueVals, unique_index] = unique(larvae.population(larvae.idx(k),:));
        idxRep = setdiff(1:env.nVars, unique_index);
        if (sum(idxRep) > 0)
            exit = 0;
            while(exit == false)
                if(sum(idxRep) == 0)
                    exit = true;
                else
                    if (sum(idxRep) > 0)
                        larvae.population(larvae.idx(k), idxRep) = ...
                            (env.varBdry(2, idxRep) - env.varBdry(1, idxRep)).*rand(1, length(idxRep)) + env.varBdry(1,idxRep);
                    end
                end
                [unique_values, unique_index] = unique(larvae.population(coral.idx(k),:));
                idxRep = setdiff(1:env.nVars, unique_index);
            end
        end
    end

end