%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 


% Define the string representing the array
inputString = '[1, 3, 3, 4]';

% Remove the brackets from the string
inputString = strrep(inputString, '[', ''); % Remove opening bracket
inputString = strrep(inputString, ']', ''); % Remove closing bracket

% Convert the string to numerical array
numericArray = str2num(inputString);

% Display the numerical array
disp(numericArray);
