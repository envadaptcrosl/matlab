%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

function [env, coral, algae] = AlgaeUpdate(env, coral, algae)
    algae.RDiscount = round((algae.maxR/3)*rand(length(algae.R), 1) );
    if(algae.RDiscount < 1)
        algae.R = algae.R - algae.RDiscount; % decrease the amount of lifes for each alga
    else
        algae.R = algae.R - 1; % decrease the amount of lifes for each alga
    end
    deathAlgaeIdx = find(algae.R <= 0); % index of the death algae
    algae.R(deathAlgaeIdx) = NaN; % NaN value for that algae life position
    algae.mask(deathAlgaeIdx) = 0; % release free position of the coral
    coral.fitness(env.currentIter, deathAlgaeIdx) = env.emptyPosVal; % assign worst value to position to be replace easily by a coral

   
    %% Update distance between algae and corals
    coral.algaeDistance = AlgaeDistance(algae); % distance between corals and algae
    coral.positions = find(algae.mask == 0);

    algae.size = length( find(algae.mask == 1) );
    %% Update parameters of acidification regulation
    if env.oceanAcidification == true
        if(algae.size > 0)
            for k = 1:1:length(coral.positions)
                % x = (sum(1 - (coral.algaeDistance(k,:) )/(env.reefSize) ))/ algae.size;
                % sig(k) = (sigmoid(x))*algae.size/env.reefSize;
                x = (sum(length(algae.mask) - coral.algaeDistance(k,:)))/length(algae.mask);
                sig(k)=(2*sigmoid(x)-1)*length(find(algae.mask==1))/length(algae.mask);
            
            env.H(coral.positions(k)) = env.H0-2.53*10^-8*sig(k);
            env.pH(coral.positions(k)) = -log10(env.H(coral.positions(k)));
            end
        else
            sig = zeros(1, length(coral.positions));
            env.H(coral.positions) = env.H0-2.53*10^-8*sig;
            env.pH(coral.positions) = -log10(env.H(coral.positions));
        end
    end
    %% Update rooting probabilities

    % Step 1: Find algae positions in the reef
    algae.positions = [];
    algae.positions = find(algae.mask);
    
    % Step 2: null probability of depredation vector
    algae.pd = zeros(1, length(algae.mask));
    
    % Step 2: update probability of depredation vector
    for k = 1:length(algae.positions)
        algae.pd = algae.pd + normpdf(1:1:env.reefSize, algae.positions(k), algae.sigma);
    end

    algae.pd = algae.pd / max(algae.pd);
    algae.posRecord(env.currentIter, :) = algae.mask;

end
