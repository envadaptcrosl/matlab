%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

function [larvae] = budding(env, coral, larvae)
    coral.idx = find( sum(isnan(coral.population), 2) == 0 );
    
    idxBudding = randperm( round( coral.Fa*rand(1,1)*length(coral.idx) ) ); % Select number of fragmented larvae
    if (~isempty(idxBudding))
        larvae.budding = coral.population(idxBudding, :); % fragmentation of larvae
        larvae.population = [larvae.population; larvae.budding]; % mix larvae populations of substrates and budding
        buddingSetAttemps = randi(1, size(larvae.budding, 1), 1); % generate random new set attemps for budding larvae
        larvae.setAttemps = [larvae.setAttemps; buddingSetAttemps]; % merge set attemps of total larvae population
        buddingFit = EvaluatePopulation(env, larvae.budding);
        larvae.fitness = [larvae.fitness buddingFit].';
        larvae.szBuddingPopulation = size(larvae.population, 1);
    else
        larvae.szBuddingPopulation = size(larvae.population, 1);
    end
end