%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function tracks the participation of the different substrates in %
% the larvae generation and it updates the probability values for each  %
% substrate layer                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [env] = SubsUpdate(env, coral)
    coral.idx = find( sum( isnan(coral.population), 2) == 0 );
    population = coral.population(coral.idx, :);
    env.coralsVariety = unique(round(population, 7),'rows'); % variety of different corals (rows)
    subsWeight = NaN * ones(1, 5);
    remainSubsRank = env.subsTable.subsRank;
    env.probSubsTypeX = 0;
    if(~isempty(env.subsX))
        subsWeight = sum(env.subsTable.subsRank(env.subsXIdx) )/ sum(env.subsTable.subsRank );
        env.probSubsTypeX = env.existSubs(1) * (1 - (env.reefSize - size(env.coralsVariety, 1))/env.reefSize); % new probabilities of cross subs
        remainSubsRank = env.subsTable.subsRank;
        remainSubsRank(env.subsXIdx) = [];
    end
    if(~isempty(env.subsMut))
        subsWeight = sum(env.subsTable.subsRank(env.subsMutIdx) )/ sum(remainSubsRank);
        env.probSubsTypeMut = subsWeight *env.existSubs(2) * (1 - env.probSubsTypeX); % new probabilities of mutation subs
    end
    if(~isempty(env.subsSwarm))
        subsWeight = sum(env.subsTable.subsRank(env.subsSwarmIdx) )/ sum(remainSubsRank );
        env.probSubsTypeSwarm =  subsWeight * env.existSubs(3) * (1 - env.probSubsTypeX); % new probabilities of mutation subs
    end % new probabilities of swarm subs

    if(~isempty(env.subsPhys))
        subsWeight = sum(env.subsTable.subsRank(env.subsPhysIdx) )/ sum(remainSubsRank );
        env.probSubsTypePhys = subsWeight * env.existSubs(4) * (1 - env.probSubsTypeX); % new probabilities of mutation subs
    end % new probabilities of physical-based subs


    if(~isempty(env.subsHum))
        subsWeight = sum(env.subsTable.subsRank(env.subsHumIdx) )/ sum(remainSubsRank)
        env.probSubsTypeHum = subsWeight * env.existSubs(5) * (1 - env.probSubsTypeX); % new probabilities of mutation subs
    end % new probabilities of human-based subs

   


    % env.subsTypeWeight = subsWeight(~isnan(subsWeight));
    % env.probSubsTypeX = env.existSubs(1) * (1 - (env.reefSize - size(env.coralsVariety, 1))/env.reefSize); % new probabilities of cross subs
    % env.probSubsTypeSwarm =  env.existSubs(2) * (1 - env.probSubsTypeX)/(sum(env.existSubs)-1); % new probabilities of swarm subs
    % env.probSubsTypeMut = env.existSubs(3) * (1 - env.probSubsTypeX)/(sum(env.existSubs)-1); % new probabilities of mutation subs
    % env.probSubsTypePhys = env.existSubs(4) * (1 - env.probSubsTypeX)/(sum(env.existSubs)-1); % new probabilities of physical-based subs
    % env.probSubsTypeHum = env.existSubs(5) * (1 - env.probSubsTypeX)/(sum(env.existSubs)-1); % new probabilities of human-based subs
    % 

    
    % env.subsTypeWeight = [0.8 1 1 1 1];
    % env.probSubsTypeX = env.existSubs(1).*...
    %     (1-env.subsTypeWeight(1)*(env.reefSize - size(env.coralsVariety, 1))/env.reefSize); % new probabilities of cross subs    
    % env.probSubsTypeMut = env.subsTypeWeight(2)*env.existSubs(2).*...
    %     (1 -  env.probSubsTypeX)/length(find(env.existSubs(2:end)));  % mutation substrates type probability initialization
    % env.probSubsTypeSwarm = env.subsTypeWeight(3)*env.existSubs(3).*...
    %     (1 -  env.probSubsTypeX)/length(find(env.existSubs(2:end)));  % swarm substrates type probability initialization
    % env.probSubsTypePhys = env.subsTypeWeight(4)*env.existSubs(4).*...
    %     (1 -  env.probSubsTypeX)/ 2 / length(find(env.existSubs(2:end))); % physical-based substrates type probability initialization
    % env.probSubsTypeHum = env.subsTypeWeight(5)*env.existSubs(5).*...
    %     (1 -  env.probSubsTypeX) / 2 /length(find(env.existSubs(2:end))); % human-based substrates type probability initialization

    % sum of probabilities must be rounded due to accumulative error in
    % precalculation processes
    if (round(env.probSubsTypeX + env.probSubsTypeSwarm + env.probSubsTypeMut + env.probSubsTypePhys + env.probSubsTypeHum,5) == 1 )
        if(~isempty(env.subsX))
            %%% gloal probability for each cross substrate
            env.probSubsX = env.probSubsTypeX * env.subsTable.subsRank(env.subsXIdx)/sum(env.subsTable.subsRank(env.subsXIdx));
        end
        if(~isempty(env.subsMut))
            %%% global probability for each mutation substrate:
            env.probSubsMut = env.probSubsTypeMut * env.subsTable.subsRank(env.subsMutIdx)/sum(env.subsTable.subsRank(env.subsMutIdx));
        end
        if(~isempty(env.subsSwarm))
            %%% global probability for each swarm substrate:
            env.probSubsSwarm = env.probSubsTypeSwarm * env.subsTable.subsRank(env.subsSwarmIdx) / sum(env.subsTable.subsRank(env.subsSwarmIdx));
        end
        if(~isempty(env.subsPhys))
            %%% global probability for each physical-based substrate:
            env.probSubsPhys = env.probSubsTypePhys * env.subsTable.subsRank(env.subsPhysIdx) / sum(env.subsTable.subsRank(env.subsPhysIdx));
        end
        if(~isempty(env.subsHum))
            %%% global probability for each physical-based substrate:
            env.probSubsHum = env.probSubsTypeHum * env.subsTable.subsRank(env.subsHumIdx) / sum(env.subsTable.subsRank(env.subsHumIdx));
        end
    else
        error("ERROR: sum of the substrates' probabilities is higher than 1")
    end
    env.subsProb = cat(1, env.probSubsX, env.probSubsMut, env.probSubsSwarm, env.probSubsPhys, env.probSubsHum); % array of all subs probabilities
    env.subsTable.subsProb = env.subsProb;
    env.subsAssign = randsrc(env.reefSize, 1, [1, 2, 3, 4, 5;...
        env.probSubsTypeX, env.probSubsTypeMut, env.probSubsTypeSwarm, env.probSubsTypePhys, env.probSubsTypeHum]);
    env.fitnessCallsEff(env.currentIter) = sum(env.subsTable.subsRank);
end