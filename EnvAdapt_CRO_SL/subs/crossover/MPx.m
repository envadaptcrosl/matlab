%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

function larvaePopulation = MPx(env, population)
    env.nXPoints = randi( round(env.nVars*0.3), 1,1 );
    % env.crossProb: corals selection parameter
    larvaePopulation = [];
    populationSize = size(population, 1); % size of population associated to the substrate operator
%     idxPopNoNaN = find(~isnan(population(:, 1)));
    idxPopNaN = find(isnan(population(:, 1)));
%     population = population(idxPopNoNaN, :);
    VarSize = size(population, 2); % number of variables
    for (j = 1:1:populationSize)
        if ( sum( find(j == idxPopNaN) ) > 0 );
            larvaePopulation(j,:) = population(j, :); % add best larva to the population
        else
            coral_1 = population(j, :); % Main coral to cross
            idxPopNoNaN = find(~isnan(population(:, 1)));
            candidatesPopulation = population; % candidates population
            candidatesPopulation(j, :) = []; % removal of coral_1 from candidates population
            idxPopNoNaN = find(~isnan(candidatesPopulation(:, 1)));
            candidatesPopulation = candidatesPopulation(idxPopNoNaN , :);
            if (size(candidatesPopulation, 1) > 0)
                coral_2 = candidatesPopulation( randi(size(candidatesPopulation, 1), 1), : ); % Selection of candidate for the crossover
            else
                coral_2 = env.gBest;
            end
            %% CROSSOVER PHASE

            crossoverPoints = randi(env.nVars, env.nXPoints, 1); % Selection of crossover points
            crossoverPoints = sort(crossoverPoints); % Sort of different crossover points
            crossoverPoints = [1; crossoverPoints; env.nVars]; % array of crossover points

            for k = 1:1:( env.nXPoints + 1 )
                if (rem(k, 2) == 0)
                    offspring( 1, crossoverPoints(k):crossoverPoints(k+1) ) = ...
                        [ coral_1(crossoverPoints(k):crossoverPoints(k+1) ) ];
                    offspring( 2, crossoverPoints(k):crossoverPoints(k+1) ) = ...
                        [ coral_2(crossoverPoints(k):crossoverPoints(k+1) ) ];   
                else
                    offspring( 1, crossoverPoints(k):crossoverPoints(k+1) ) = ...
                        [ coral_2( crossoverPoints(k):crossoverPoints(k+1) ) ];
                    offspring( 2, crossoverPoints(k):crossoverPoints(k+1) ) = ...
                        [ coral_1( crossoverPoints(k):crossoverPoints(k+1) ) ];
                end          
            end


            %% MUTATION PHASE
    %         if (rand() < env.mutationLarvaProb)
    %             mutPos = randi(VarSize, 1, round(env.mutationGenProb * VarSize));
    %             offspring(1, mutPos) = env.varBdry(1, mutPos) + (env.varBdry(2, mutPos) - env.varBdry(1, mutPos))...
    %                 *rand(env.varBdry(2, mutPos), 1, size(mutPos(1, :),2));
    %         end
    %         if (rand() < env.mutationLarvaProb)
    %             mutPos = randi(VarSize, 1, round(env.mutationGenProb * VarSize));
    %             offspring(2, mutPos) = env.varBdry(1, mutPos) + (env.varBdry(2, mutPos) - env.varBdry(1, mutPos))...
    %                 *rand(env.varBdry(2, mutPos), 1, size(mutPos(1, :),2));
    %         end

            %% TOURNAMENT PHASE

            fit = EvaluatePopulation(env, offspring);

            % Basic tournament
            if env.trend == "min"
                if fit(1) > fit(2) % minimization problem
                    larva = offspring(2, :); % second larva is better
                else
                    larva = offspring(1, :); % first larva is better
                end
            else
                if fit(1) < fit(2) % minimization problem
                    larva = offspring(2, :); % second larva is better
                else
                    larva = offspring(1, :); % first larva is better
                end
            end
            larvaePopulation(j,:) = larva; % add best larva to the population
        end
    end
end