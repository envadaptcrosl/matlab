%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

%% Substrate: Covariance Matrix Adaption
%%% Type: Mutation

function [larvaePopulation] = CMA(env, population)

    for k = 1:1:size(population, 1)
        %% Parameters update - Cumulation step for evolution paths
        parent = population(k, :); % parent selection (initial mean point)
    
        newParent = parent * env.weights; % parent values weight
        diffParents = newParent - parent; % difference between new and original parents
        env.pathSigma = (1 - env.Cs) * env.pathS + sqrt(env.Cs * (2 - env.Cs) * env.muEff) ...
            env.invsqrtC * diffParents; % update of sigma evolution path
        hSigma = norm(env.pathSigma)/sqrt(1 - (1 - env.Cs)^(2*k/lambda))/env.chiVars < (1.4 +2/(nVars + 1)), % boolean value
        env.pathC = (1 - env.Cc) * env.PathC + hSigma*sqrt(env.Cc*(2 - env.Cc)*env.muEff) * diffParents/env.sigma; %update fo cumulation evolution path
        
        %% Adaptation of covariance matrix covMatrix
        
        env.covMatrix = (1 - env.cma_c1 - env.Cmu) * env.covMatrix + env.cma_c1 * (env.pathC*env.pathC' + (1 - env.hSigma) * env.Cc*(2 - env.Cc) * ) ...
            
        
        %% Larvae generation
        for t = 1:1:lambda
            offspring(t, :) = round(newParent + env.sigma * env.B * (D.*randn(1, nVars)));
            idxUnderBdry = find(offspring(t, :) < env.varBdry(1,:));
            offspringFit(t) = env.objFunction(env, offspring(t, :));
        end
%         BoundRestriction(env, offspring)
%         if length(idxUnderBdry > 0)
%             offspring(k, idxUnderBdry) = env.varBdry(1, idxUnderBdry);
%         end
%         idxOverBdry = find(offspring(k, :) > env.varBdry(2, :));
%         if length(idxOverBdry > 0)
%             offspring(k, idxOverBdry) = env.varBdry(2, idxOverBdry);
%         end
        if (env.problemType == "max") % sort offspring fitness values for maximization problem
            [~, idxSortOffspring] = sort(offspringFit, "descend"); 
        elseif (env.problemType == "min") % sort offspring fitness values for minimization problem
            [~, idxSortOffspring] = sort(offspringFit, "ascend");
        else
            error("ERROR: optimization trend 'env.trend' not supported."); % Not supported trend :(
        end
        larvaePopulation(k, :) = offspring(idxSortOffspring, :); % assign best offspring to larvae population
    end
end