%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Substrates based on the papers: 
% 
% BLXa
% M. Takahashi and H. Kita,
% "A crossover operator using independent component analysis for real-coded genetic algorithms," 
% Proceedings of the 2001 Congress on Evolutionary Computation (IEEE Cat. No.01TH8546), 
% Seoul, Korea (South), 2001, pp. 643-649 vol. 1, doi: 10.1109/CEC.2001.934452.
% +info: https://ieeexplore.ieee.org/document/934452 
% -----------------------------------------------------------------------------------------------
% BLXa-b
% Herrera F. , Lozano M. , Sánchez A.M.  (2003), 
% "A taxonomy for the Crossover Operator for Real-Coded Genetic Algorithms: An Experimental Study, in International Journal of Intelligent Systems",
% Wiley, vol. 18, pp. 309-338
% +info: https://onlinelibrary.wiley.com/doi/abs/10.1002/int.10091
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [larvaePopulation, larvaeFitness] = BLX(env, population) % Blended Crossover
    populationSize = size(population, 1); % size of the population fraction held in this substrate
    larvaePopulation = zeros(populationSize, size(population, 2)); % initialize larvae population
    larvaeFitness = zeros(populationSize, 1); % initialize larvae fitness

    for k = 1:populationSize
        parent1 = population(k, :); % First parent coral
        candidatesPopulation = population; % candidates population
        candidatesPopulation(k, :) = []; % deletion of first parent from candidates population
        parent2 = candidatesPopulation(randi(populationSize-1, 1), :); % Selection of candidate for the crossover

        typeBLX = randi(2); % selection of BLX type
        offspring = zeros(2, env.nVars); % initialize offspring

        if (typeBLX == 1) % BLX-alpha substrate
            alpha = env.BLXa;
            for m = 1:env.nVars
                d = abs(parent1(m) - parent2(m)); % distance between two variables
                lb = min(parent1(m), parent2(m)) - alpha * d; % lower boundary
                ub = max(parent1(m), parent2(m)) + alpha * d; % upper boundary
                offspring(:, m) = lb + (ub - lb) * rand(2, 1); % generate offspring
            end
        else % BLX-alpha-beta substrate
            alpha = env.BLXa;
            beta = env.BLXb(env.currentIter);
            for m = 1:env.nVars
                d = abs(parent1(m) - parent2(m)); % distance between two variables
                lb = min(parent1(m), parent2(m)) - alpha * d; % lower boundary
                ub = max(parent1(m), parent2(m)) + beta * d; % upper boundary
                offspring(:, m) = lb + (ub - lb) * rand(2, 1); % generate offspring
            end
        end

        % Evaluate offspring
        fit = EvaluatePopulation(env, offspring);

        % Tournament phase
        if env.trend == "min"
            if fit(1) < fit(2)
                larvaePopulation(k, :) = offspring(1, :); % select better offspring
                larvaeFitness(k) = fit(1);
            else
                larvaePopulation(k, :) = offspring(2, :); % select better offspring
                larvaeFitness(k) = fit(2);
            end
        else
            if fit(1) > fit(2)
                larvaePopulation(k, :) = offspring(1, :); % select better offspring
                larvaeFitness(k) = fit(1);
            else
                larvaePopulation(k, :) = offspring(2, :); % select better offspring
                larvaeFitness(k) = fit(2);
            end
        end
    end
end
