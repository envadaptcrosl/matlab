%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

%% Substrate: Random Mutation
%%% Type: Mutation

function [larvaePopulation] = RandM(env, population)
    % mutation process of variables
    popSize = size(population, 1);
    larvaePopulation = population;
    for k = 1:1:popSize
        idxMutation = randperm(env.nVars, round(env.mutationVarProb*rand*env.nVars) );
        larvaePopulation(k, idxMutation) = env.varBdry(1, idxMutation) +...
            ( env.varBdry(2, idxMutation) - env.varBdry(1, idxMutation) ).*rand( 1, length(idxMutation) );
    end
end