%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

function [larvaePopulation] = TS(env, population)
    % env.tabuList = {};
    bestSolution = population(1, :);

    if env.trend == "max"
        bestFitness = -inf;
    else
        bestFitness = inf;
    end
    
    for i = 1:size(population, 1)
        currentSolution = population(i, :);
        bestNeighbor = currentSolution;
        bestNeighborFitness = EvaluateIndividual(env, currentSolution);
        
        % search for the best neighbor
        for j = 1:10 % number of neighbors to consider
            neighbor = currentSolution + (-1 + 2 * rand(1, env.nVars)); % Generate neighbor
            neighbor = max(env.varBdry(1, :), min(env.varBdry(2, :), neighbor)); % Bound the neighbor within variable limits
            % check if neighbor is in tabu list
            if ~ismember(neighbor, env.tabuList, 'rows')
                neighborFitness = EvaluateIndividual(env, neighbor);
                
                % update the best neighbor
                if env.trend == "min"
                    if neighborFitness < bestNeighborFitness
                        bestNeighbor = neighbor;
                        bestNeighborFitness = neighborFitness;
                    end
                else
                    if neighborFitness > bestNeighborFitness
                        bestNeighbor = neighbor;
                        bestNeighborFitness = neighborFitness;
                    end
                end
            end
        end
        
        % add the best neighbor to tabu list
        env.tabuList{end+1} = bestNeighbor;
        if length(env.tabuList) > env.maxTabuSize
            env.tabuList(1) = [];
        end
        
        % update best solution
        if env.trend == "min"
            if bestNeighborFitness < bestFitness
                bestSolution = bestNeighbor;
                bestFitness = bestNeighborFitness;
            end
        else
            if bestNeighborFitness > bestFitness
                bestSolution = bestNeighbor;
                bestFitness = bestNeighborFitness;
            end
        end
        
        larvaePopulation(i, :) = bestNeighbor;
    end
end