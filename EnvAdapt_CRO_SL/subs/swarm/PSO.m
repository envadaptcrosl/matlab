%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

function [population, psoVel] = PSO(env, population, xBest, psoVel)
    %% Update movement reference values
%     psoIdx = find(larvae.assignment(env.idxSwarm) == 1); % get index of corals subject to PSO substrate
    
    gBest = env.gBest(env.currentIter, :);
    
    %% Larvae movement phase
    for k = 1:1:size(population, 1)
        psoVel(k, :) = env.psoW(env.currentIter) .* psoVel(k, :) +...
            env.psoC1 .* ( gBest - population(k, :) ) + ...
            env.psoC2 .* (xBest(k,:) - population(k, :)); % calculate velocity
        psoPosition = population(k, :) + psoVel(k, :); % calculate new larvae position
        psoPosition = BoundConstraint(env, psoPosition); % check variables upper and lower boundaries
        population(k, :) = psoPosition; % update restricted position values
    end
    
 end