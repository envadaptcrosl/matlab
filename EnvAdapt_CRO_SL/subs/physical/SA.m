%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

%% Substrate: Simulated Annealing
%%% Type: Physical-based

function [larvaePopulation] = SA(env, population)

    T = env.alphaSA^(env.currentIter)* env.T0; % decrease initial temperature
    
    % var modification boundary limit values = [-1 1]
    modValues =  -ones(size(population, 1), env.nVars) + 2* rand(size(population, 1), env.nVars); 
    larvaePopulation = population + modValues; % first larvae generated
    fitness(1,:) = env.objFunction(env, larvaePopulation); % evaluate new larvae
    
    % var modification boundary limit values = [-1 1]
    modValues = -ones(size(population, 1), env.nVars) + 2* rand(size(population, 1), env.nVars);
    larvaeCandidates = population + modValues; % candidates to substitute first gen of larvae
    fitness(2,:) = env.objFunction(env, larvaeCandidates);
    delta = fitness(1,:) - fitness(2,:); 
    
    betterIdx = find(delta > 0);
    worseIdx =  find(delta < 0);
    if delta > 0
        larvaePopulation(betterIdx) =  larvaeCandidates(betterIdx);
    end
    
    if delta < 0
        r = rand(length(worseIdx), 1);
        P = exp(-delta/T) % probability of setting in larvae population with worse fitness
        if (P > r)
            larvaePopulation(worseIdx) =  larvaeCandidates(worseIdx);
        end
    end
end