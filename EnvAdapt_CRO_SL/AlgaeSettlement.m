%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

function [env, coral, algae] = AlgaeSettlement(env, coral, algae) 
    algae.size = sum(algae.mask);

    emptyPos = find(coral.fitness(env.currentIter-1, :) == env.emptyPosVal);

    if (algae.size < algae.maxNumber)
        r = rand(1, length(emptyPos) ); % generate a random numbers to rooting probabilities
        rootPos = find(r < algae.rootingProb); % rooting position in the reef
        coral.fitness(env.currentIter, emptyPos(rootPos)) = NaN;
        % algae.R( emptyPos(rootPos) ) = algae.maxLifes; % assign max lifes to new algae


        algae.R( emptyPos(rootPos) ) = AlgaeResistance(algae, emptyPos(rootPos));
        algae.mask(emptyPos(rootPos)) = 1; % insert algae position in the mask
    end
    coral.idx =  find( sum( isnan(coral.population), 2 ) == 0 ); % index of holded positions of the coral
end
