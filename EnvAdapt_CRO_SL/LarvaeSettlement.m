%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

function [env, coral] = LarvaeSettlement(env, coral, larvae)
    %larvae.fitness = evaluatePopulation(larvae.population);
    % Search for a position in the coral
    for larva = 1:1:larvae.szBuddingPopulation % loop for each generated larva
        % discount factor decrease the rooting probabilities
        discFactor = 1 - length( find( ismember(coral.population, larvae.population(larva,:),'rows') ) )./env.reefSize;
        for attemp = 1:1:larvae.setAttemps(larva) % loop of settlement trying for the larva
            setPosition = randi(env.reefSize,1,1); % select a random position of the array to try the set process
           
            %% Tournament phase
            r = rand(); % r: rooting probability of larva
            if env.trend == "min"
                if (larvae.fitness(larva) < coral.fitness(env.currentIter, setPosition)) && ( r < larvae.rootProb * discFactor ) ...
                         & sum( coral.population(setPosition, :) == larvae.population(larva, :) ) ~= env.nVars
                    coral.population(setPosition, :) = larvae.population(larva, :); % replace coral for a larva
                    coral.fitness(env.currentIter, setPosition) = larvae.fitness(larva); % replace fitness
                    if ( (larvae.szPopulation - larva) >= 0 )
                        env.subsTable.subsRank( larvae.assignment(larva) ) = env.subsTable.subsRank( larvae.assignment(larva) ) + 1;
                        % add +1 to the substrate which origined the larva in the rank
                    end
                    attemp = larvae.setAttemps(larva); % finish attemps
                end
            else
                if (larvae.fitness(larva) > coral.fitness(env.currentIter, setPosition)) && ( r < larvae.rootProb * discFactor ) ...
                         & sum( coral.population(setPosition, :) == larvae.population(larva, :) ) ~= env.nVars
                    coral.population(setPosition, :) = larvae.population(larva, :); % replace coral for a larva
                    coral.fitness(env.currentIter, setPosition) = larvae.fitness(larva); % replace fitness
                    if ( (larvae.szPopulation - larva) >= 0 )
                        env.subsTable.subsRank( larvae.assignment(larva) ) = env.subsTable.subsRank( larvae.assignment(larva) ) + 1;
                        % add +1 to the substrate which origined the larva in the rank
                    end
                    attemp = larvae.setAttemps(larva); % finish attemps
                end
            end
        end    
    end
    coral.idx =  find( sum( isnan(coral.population), 2 ) == 0 ); % index of holded positions of the coral
    if env.trend == "min"
        [~, idxBestLarva] = min(larvae.fitness); % get best larva index
    else
        [~, idxBestLarva] = max(larvae.fitness); % get best larva index
    end
    if idxBestLarva <= length(larvae.assignment)
        idxBestLarvaSubs = larvae.assignment(idxBestLarva); % search for the substrate that generates the larva
        env.subsTable.subsBest(idxBestLarvaSubs) = env.subsTable.subsBest(idxBestLarvaSubs) + 1; % sum +1 to the best subs counter
    end
end
