%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 


function larvae = SubsMutation(env, larvae, population)
    idxCoral = find( sum( isnan(population), 2 ) == 0 ); % get index of coral positions in the reef
    population = population(idxCoral, :); % remove non coral positions (free or algae sites)
    if (sum(env.probSubsMut) > 0) && (sum(env.probSubsTypeMut) > 0) && (size(population, 1) > 0)
        internalAssign = randsrc(size(population, 1), 1, [env.subsMutIdx.'; round((env.probSubsMut./env.probSubsTypeMut),15).']);
        %% Calculate new larvae from an specific substrate
        for k = 1:1:length(env.subsMutIdx)
            fun = str2func( env.subsTable.subs( env.subsMutIdx(k) ) ); % set function name
            population( find(internalAssign == env.subsMutIdx(k)), : ) = ...
                fun( env, population( find( internalAssign == env.subsMutIdx(k) ), :) );
            assignment( find(internalAssign == env.subsMutIdx(k) ) ) = env.subsMutIdx(k);
        end
        larvae.population = [larvae.population; population];
        larvae.assignment = [larvae.assignment assignment];
    end
end
