%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

function fitness = EvaluatePopulation(env, population)
    if (env.varType == "int")
        population = round(population);
    end
    for k = 1:1:( size(population, 1) ) % for each individual in the population...
        if ( ~isnan(population(k, :)) )
            %% PLEASE CHANGE YOUR FITNESS FUNCTION NAME HERE!
            %fitness(k) = FitnessFunction_Test( population(k, :) ); % calculate it fitness value
            fitness(k) = env.objFunction(env, population(k, :));
        else
            fitness(k) = NaN;
        end
    end
end