%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

clear
%% MAIN SCRIPT (EnvAdapt-CRO-SL): Execute this script to start!

%%% Initialization: problem structures and properties
%%%-------------------------------------------------
addpath("./init"); addpath("./utils");
addpath("./subs/mutation"); addpath("./subs/swarm");
addpath("./subs/physical"); addpath("./subs/crossover");
addpath("./subs"); addpath("./fitness_function");
addpath("./fitness_function/examples");


env = struct; % definition of structure
env = GenEnv(env); % definition of 'env' structure
if (exist("env.seed","var") && exist("env.prng", "var"))
    rng(env.seed, env.prng) % set seed, comment if you want random seed
end
env.prngdata = rng;
coral = GenCoral(env); % definition of 'coral' structure

env.xBest = coral.population;
env.xBestFit = coral.fitness;
% copy the best coral results in the 'env' struct / swarm substrates references
env.gBest(1, :) = coral.bestCoral(1, :);
env.gBestFit(1, :) = coral.bestFitness(1);

% update xBest and gBest values
env = UpdateParamPSO(env, coral);

if (env.oceanAlgae == true)
    algae = GenAlgae(env);
else
    algae = struct
end
prjName = "envadapt-prj1"; % set project name to save results
savedFilename = "results/" + prjName + ".mat"; % path to save results
% Type of variables definition


env = UpdateParamPSO(env, coral);

for currentIter = 2:1:env.nIterations
    
    env.currentIter = currentIter; % update iteration value
    % copy the best coral results in the 'env' struct / swarm substrates references
    env.gBest(currentIter, :) = coral.bestCoral(currentIter-1,:);
    env.gBestFit(currentIter) = coral.bestFitness(currentIter-1);
     %% Algae settlement procedure
    if (env.oceanAlgae == true) % if algae mode is ON...
        [env, coral, algae] = AlgaeUpdate(env, coral, algae); % algae status update
        [env, coral, algae] = AlgaeSettlement(env, coral, algae); % algae settlement
    end

    %% Fitness acidification procedure
    if (env.oceanAcidification == true) && (env.currentIter ~= 1) % if acidification is ON...
        [env, coral] = Acidification(env, coral); % decreasing in the coral fitness values
    else
        % copy fitness to struct in actual iteration (they will be taken as reference in Larvae Settings)
        coral.fitness(env.currentIter, :) = coral.fitness(env.currentIter-1, :); 
    end
  
    %% Larvae settlement procedure
    [env, larvae] = GenLarvae(env, coral); % generation of new larvae
    larvae = Budding(env, coral, larvae); % fragmentation of corals (new larvae)
    [env, coral] = LarvaeSettlement(env, coral, larvae); % larvae settlement
    [env, coral] = NumberOfDetectedSites(env, coral);
    if env.trend == "max"
        % get max fitness value and index of best coral
        [coral.bestFitness(currentIter), bestIdx] = max(coral.fitness(currentIter, :));
    else
        % get min fitness value and index of best coral
        [coral.bestFitness(currentIter), bestIdx] = min(coral.fitness(currentIter, :));
    end
    coral.bestCoral(currentIter,:) = coral.population(bestIdx,:); % update best coral history
    
    
    %% Summary of solutions display
    if (env.consoleTrace == true) % if console trace is activate, plot results in console
        disp(env.subsTable);
        disp("Iteration: " + env.currentIter);
        plotMaxFit = min( coral.fitness(env.currentIter, :));
        disp("Best Fitness: " + plotMaxFit );
        plotMeanFit(env.currentIter) = mean( coral.fitness(env.currentIter, coral.idx) );
        disp("Average Fitness: " +  plotMeanFit(env.currentIter));
%        disp(sum(algae.mask));
        disp('__________________________________________________________');
    end

    %% Population update procedure
%     [env, coral] = UpdateParamPSO(env, coral)
    coral = CoralUpdate(env, coral, algae); % release of new coral 
    env = SubsUpdate(env, coral); % release of new substrate layers

end

%% Results save '.mat' file
% save(savedFilename);
if env.trend == "max"
    [coral.opt, coral.idxOpt] = max(max(coral.fitness,[],2));
else
    [coral.opt, coral.idxOpt] = min(min(coral.fitness,[],2));
end
totalCalls = sum(env.fitnessCalls(1:coral.idxOpt))
effectiveCalls = env.fitnessCallsEff(coral.idxOpt);
disp("Total de llamadas:")
disp(totalCalls)
disp("Eficiencia:")
disp(effectiveCalls/totalCalls)
disp("Optimum index")
disp(coral.idxOpt)

