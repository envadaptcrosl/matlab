%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

function [env, coral] = Depredation(env, coral, algae)

    coral.idx =  find( sum( isnan(coral.population), 2 ) == 0 ); % index of holded positions of the coral

    population = coral.population( coral.idx, : ); % remove algae and free positions
    fitness = coral.fitness(env.currentIter, coral.idx ); % remove algae and free positions
    if(env.varType == "int")
        [uniqueCorals, idxUnique, idxRep] = unique(round(population, 3), 'rows'); % get unique corals of the reef
        deathCandidates = setdiff(1:size(population, 1), idxUnique); % identify repeated corals
    else
        [uniqueCorals, idxUnique, idxRep] = unique(round(population,6), 'rows'); % get unique corals of the reef
        deathCandidates = setdiff(1:size(population, 1), idxUnique); % identify repeated corals
    end
    if (env.oceanAlgae)
        algae_positions = find(algae.mask); % find algae positions
        
        % Gaussian distribution of death probabilities
        for k = 1:length(algae_positions)
            algae.pd = normpdf(1:length(algae.mask), algae_positions(k), algae.sigma);
        end
         
        algae.pd = algae.pd / max(algae.pd); % normalization of the probability of depredation
        r = algae.pd(deathCandidates);
    else
        r = rand( length(deathCandidates), 1 ); % random values for selection
        
    end
    deathCorals = r > coral.deathRate; % random selection between repeated corals
    deathIdx = deathCandidates( find(deathCorals) ); % index of the death corals
    population(deathIdx, :) = NaN * zeros( length(deathIdx), env.nVars ); % free the position of the reef
    coral.population(deathIdx, :) = NaN * zeros( length(deathIdx), env.nVars ); % free the position of the reef
    coral.fitness(env.currentIter, deathIdx) = env.emptyPosVal; % update fitness value of empty position
    coral.idx =  find( sum( isnan(coral.population), 2 ) == 0 ); % index of holded positions of the coral
end