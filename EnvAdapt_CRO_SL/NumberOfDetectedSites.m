%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

function [env, coral] = NumberOfDetectedSites(env, coral)

    coral.idx =  find( sum( isnan(coral.population), 2 ) == 0 ); % index of holded positions of the coral
    coral.NaN = find( sum( isnan(coral.population), 2 ) == env.nVars );
    
    coral.fitVal = find( isnan( coral.fitness(env.currentIter, :) ) == 0 );
    coral.fitNaN = find( isnan( coral.fitness(env.currentIter, :) ) );
    coral.fitInf = find( coral.fitness(env.currentIter, :) == env.emptyPosVal );
    coral.fitVal = find( isnan( coral.fitness(env.currentIter, :) ) == 0 );

    detectedSitesPos = length(coral.idx) + length(coral.NaN);
    detectedSitesFit = length(coral.fitVal) + length(coral.fitNaN); %+ length(coral.fitInf);

    % disp("Detected sites in positions:")
    % disp(detectedSitesPos)
    % disp("Detected sites in fitness:")
    % disp(detectedSitesFit)
end