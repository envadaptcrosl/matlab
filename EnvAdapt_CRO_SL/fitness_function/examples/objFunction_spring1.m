%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

function [objVal] = objFunction_spring1(env, x)
    constraint(1) = 1 - ( x(2)^3*x(3) )/(71785*x(1)^4) <= 0;
    constraint(2) = (4*x(2)^2 - x(1)*x(2))/(12566*(x(2)*x(1)^3-x(1)^4)) + 1/(5108*x(1)^2) - 1 <= 0;
    constraint(3) = 1 - 140.45*x(1)/( x(2)^2*x(3) ) <= 0;
    constraint(4) = (x(1) + x(2))/1.5 - 1 <= 0;

    if ( sum(constraint) == 4 )
        objVal = (x(3) + 2) * x(2) * x(1)^2;
    else
        if env.trend == "min"
            objVal = Inf; % minimization problems
        else
            objVal = -Inf; % maximization problems
        end
    end
    
end