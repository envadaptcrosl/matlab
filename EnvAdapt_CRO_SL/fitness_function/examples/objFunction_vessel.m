%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

function [objVal] = objFunction_vessel(env, x)
    constraint(1) = (-x(1) + 0.0193*x(3)) <= 0;
    constraint(2) = (-x(2) + 0.00954*x(3)) <= 0;
    constraint(3) = (-pi*x(3)^2*x(4) - 4/3*pi*x(3)^3 + 1296000) <= 0;
    constraint(4) = (x(4)-240) <= 0;

    if ( sum(constraint) == 4 )
        objVal = 0.6224*x(1)*x(3)*x(4) + 1.7781*x(2)*x(3)^2 + ...
            3.1661*x(1)^2*x(4) + 19.84*x(1)^2*x(3);
    else
        if env.trend == "min"
            objVal = Inf; % minimization problems
        else
            objVal = -Inf; % maximization problems
        end
    end
    
end