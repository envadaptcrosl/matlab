
subsNames = {};
            
            % Define folder paths
            folders = {'../subs/crossover', '../subs/mutation', '../subs/swarm', '../subs/physical', '../subs/human'};

for folderIndex = 1:length(folders)
                % Get folder path
                subsFolder = folders{folderIndex};
                
                % Get file list in the folder
                subsFiles = dir(subsFolder);
                
                % Iterate over files in the folder
                for i = 1:length(subsFiles)
                    % Check if it's a file and not a directory or hidden file
                    if ~subsFiles(i).isdir && ~startsWith(subsFiles(i).name, '.')
                        % Append file name to subsNames
                        subsNames{end+1} = subsFiles(i).name;
                    end
                end
            end