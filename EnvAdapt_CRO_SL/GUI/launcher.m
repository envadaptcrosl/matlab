%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 


function [env, coral, algae] = launcher(env, coral, app)
    %% MAIN SCRIPT (EnvAdapt-CRO-SL): Execute this script to start!
    
    %%% Initialization: problem structures and properties
    %%%-------------------------------------------------
    addpath("../init"); addpath("../");
    addpath("../subs/mutation"); addpath("../subs/swarm");
    addpath("../subs/physical"); addpath("../subs/crossover");
    addpath("../subs"); addpath("../fitness_function");
    
    % rng(5); % Fix random seed
    % rng('default'); % default random seed == rng(0, "twister");
    

    %% Rewrite of env structure with GUI data

    env.objFunction = str2func(['@',app.objFunction.Value]);
    env.nIterations = app.generations.Value; % established stop criteria for a determinated number of iterations
    env.trend = app.trend.Value; % minimization or maximization problem
    env.gBest = [];
    env.gBestFit = [];
    env.nVars = app.nVars.Value; % (M)
    env.reefSize = app.reefSize.Value; % (S) number of available positions in the reef
    env.reefFilled = round(env.reefSize*app.reefFilled.Value); % reefFilled <= reefSize
    env.filledPos = randperm(env.reefSize, env.reefFilled);
    %%% PRNG and seed selection
    if (app.twisterButton.Value == true)
        env.prng = "twister";
    elseif (app.threefryButton.Value == true)
        env.prng = "threefry";
    elseif (app.simdTwisterButton.Value == true)
        env.prng = "simdTwister";
    elseif (app.combRecursive.Value == true)
        env.prng = "combRecursive";
    elseif (app.multiFibonacciButton.Value == true)
        env.prng = "multiFibonacci";
    elseif (app.philoxButton.Value == true)
        env.prng = "philox";
    else
        env.prng = "twister"; % default prng
    end
    env.seed = app.seed.Value;
    rng(env.seed, env.prng);
    %%% Types of optimization variables
    if (app.FloatButton.Value == true)
        env.varType = "float";
    else
        env.varType = "int";
    end

    env.gBest = [];
    env.gBestFit = [];

    %%% Boundary values selection
    lowerBoundaries = str2num(app.lowerBoundaries.Value);
    upperBoundaries = str2num(app.upperBoundaries.Value);
    if (length(lowerBoundaries) == 1) && (length(upperBoundaries) == 1)
        env.varBdry = [lowerBoundaries; upperBoundaries].*ones(2,env.nVars); % Max and min boundaries
    else
        env.varBdry = [lowerBoundaries; upperBoundaries];
    end
    if app.repeatedVals.Value == "No"
        env.repeatedVals = false; % boolean value if the population can repeat values
    else
        env.repeatedVals = true; % boolean value if the population can repeat values
    end

    %
    %% Environmental Adaptive operators
    %%%--------------------------------
    env.oceanAlgae = app.algaeON.Value;
    if env.oceanAlgae == true
        algae = GenAlgae(env);
        algae.b = app.algaeB.Value;
        algae.c = app.algaeC.Value;
        algae.rootingProb = app.P_root.Value;
        algae.sigma = round((app.algaeSigma.Value/100)*env.reefSize);
    end
    env.oceanAcidification = app.acidificationON.Value; % activation of acidification process
    if (env.oceanAcidification == true)
        if(env.oceanAlgae == true)
            env.H0 = 3.25*10^-8; % maximum H+ value
            env.Hmin = 6.31*10^-9; % mininum H+ value
            env.pHmax = -log10(env.Hmin);
            env.incrH = abs(env.H0 - env.Hmin); % range of values for H
            env.lambda = 1; % adjust factor for acidification calculation
        else
            env.acidBdry = [app.acid_min.Value app.acid_max.Value]; % boundary of fitness reduction factor per iteration
        end
    end
    %%% Real-time display
    if (app.LiveplotSwitch.Value == "On")
        env.consoleTrace = true; % display max and avg fitness results and substrates participation
    else
        env.consoleTrace = false;
    end
    
    %%% Optimization trend: maximize or minimize
    if (env.trend == "max") % Problem: maximize the objective/fitness function
        env.emptyPosVal = -Inf; % max values for empty position to be replaceable easily
    elseif (env.trend == "min") % Problem: minimize the objective/fitness function
        env.emptyPosVal = Inf; % max values for empty position to be replaceable easily
    else
        error("ERROR: optimization trend 'env.trend' not supported."); % Not trend supported
    end


     %% Substrates properties
    %%%---------------------
    env.subsX = []; % crossover substrates
    env.subsMut = []; % mutation substrates
    env.subsSwarm = []; % swarm substrates
    env.subsPhys = []; % physical-based substrates 
    env.subsHum = []; % human-based substrates
    subs = string(app.SubstrateLayersListBox.Value);
    for k=1:length(subs)
        route = which(subs(k))
        if (~isempty(strfind(route, "mutation")) == 1 )
            env.subsMut = [env.subsMut, subs(k)];
        elseif (~isempty(strfind(route, "crossover")) == 1 )
            env.subsX = [env.subsX, subs(k)];
        elseif (~isempty(strfind(route, "swarm")) == 1 )
            env.subsSwarm = [env.subsSwarm, subs(k)];
        elseif (~isempty(strfind(route, "physical")) == 1 )
            env.subsPhys = [env.subsPhys, subs(k)];
        elseif (~isempty(strfind(route, "human")) == 1 )
            env.subsHum = [env.subsHum, subs(k)];
        end
    end
    
    %%% Number of Substrates per Substrate group
    env.nSubsX = length(env.subsX); % number of cross substrates
    env.nSubsMut = length(env.subsMut); % number of mutation substrates
    env.nSubsSwarm = length(env.subsSwarm); % number of swarm substrates
    env.nSubsPhys = length(env.subsPhys); % number of physical-based substrates    
    env.nSubsHum = length(env.subsHum); % number of human-based substrates
    %%% Assembly of the substrate layers structure
    env.subsType = repelem(["Crossover", "Mutation", "Swarm", "Physical", "Human"],...
        [env.nSubsX, env.nSubsMut, env.nSubsSwarm, env.nSubsPhys, env.nSubsHum]);
    env.subs = cat(2, env.subsX, env.subsMut, env.subsSwarm, env.subsPhys, env.subsHum);
    env.nSubs = length(env.subs); % substrates size
    env.subsRank = ones(env.nSubs,1); % number of participation along the iterations (started at 0)
    env.subsBest = zeros(env.nSubs,1);
    env.existSubs = [~isempty(env.subsX) ~isempty(env.subsMut) ~isempty(env.subsSwarm) ~isempty(env.subsPhys) ~isempty(env.subsHum)];
    
    env.subsTypeProb = env.existSubs./sum( env.existSubs );
    
    
    env.probSubsTypeX = env.subsTypeProb(1);  % cross substrates type probability initialization
    env.probSubsTypeMut = env.subsTypeProb(2);  % mutation substrates type probability initialization
    env.probSubsTypeSwarm = env.subsTypeProb(3);  % swarm substrates type probability initialization
    env.probSubsTypePhys = env.subsTypeProb(4); % physical-based substrates type probability initialization
    env.probSubsTypeHum = env.subsTypeProb(5); % human-based substrates type probability initialization

    %%% global probability for each cross substrate:
    env.probSubsX = env.probSubsTypeX.*ones(length(env.subsX),1)./env.nSubsX;
    %%% global probability for each mutation substrate:
    env.probSubsMut = env.probSubsTypeMut.*ones(length(env.subsMut),1)./env.nSubsMut;
    %%% global probability for each Swarm substrate:
    env.probSubsSwarm = env.probSubsTypeSwarm.*ones(length(env.subsSwarm),1)./env.nSubsSwarm;
    %%% global probability for each physical-based substrate
    env.probSubsPhys = env.probSubsTypePhys.*ones(length(env.subsPhys),1)./env.nSubsPhys;
    %%% global probability for each human-based substrate
    env.probSubsHum = env.probSubsTypeHum.*ones(length(env.subsHum),1)./env.nSubsHum;
    env.subsProb = cat(1, env.probSubsX, env.probSubsMut, env.probSubsSwarm, env.probSubsPhys, env.probSubsHum); % array of all subs probabilities
    
    
    %% Table of substrates info
    %%%------------------------
    subs = env.subs.';
    env.subsTable = table(subs);
    env.subsTable.subsType = env.subsType.';
    env.subsTable.subsRank = env.subsRank;
    env.subsTable.subsBest = env.subsBest;
    env.subsTable.subsProb = env.subsProb;
    %env.subsTable = convertvars(env.subsTable,{'subs','subsType'},'categorical');
    if(~isempty(env.subsX))
        env.subsXIdx = find( ismember(env.subsTable.subs, env.subsX) ); % index of cross substrates in the table
    end
    if(~isempty(env.subsMut))
        env.subsMutIdx = find( ismember(env.subsTable.subs, env.subsMut) ); % index of mutation substrates in the table
    end
    if(~isempty(env.subsSwarm))
        env.subsSwarmIdx = find( ismember(env.subsTable.subs, env.subsSwarm) ); % index of swarm substrates in the table
    end
    if(~isempty(env.subsPhys))
        env.subsPhysIdx = find( ismember(env.subsTable.subs, env.subsPhys) ); % index of physical-based substrates in the table
    end
    if(~isempty(env.subsHum))
        env.subsHumIdx = find( ismember(env.subsTable.subs, env.subsHum) ); % index of human-based substrates in the table
    end
    % assignment of reef positions to a determined substrate
%     subs = 1:1:length(env.subsProb);
%     env.subsAssign = randsrc(env.reefSize, 1, [subs; env.subsProb.']);
    
    env.subsTypeProb(find( env.subsTypeProb == 0 )) = [];
    subsTypes = 1:1:length(env.subsTypeProb);
    env.subsAssign = randsrc(env.reefSize, 1, [subsTypes; env.subsTypeProb]);




    coral = GenCoral(env)
%% OPTIMIZATION PROCESS

    for currentIter = 2:1:env.nIterations

        env.currentIter = currentIter; % update iteration value
        % copy the best coral results in the 'env' struct / swarm substrates references
        env.gBest(currentIter, :) = coral.bestCoral(currentIter-1,:);
        env.gBestFit(currentIter) = coral.bestFitness(currentIter-1);
         %% Algae settlement procedure
        if (env.oceanAlgae == true) % if algae mode is ON...
            [env, coral, algae] = AlgaeUpdate(env, coral, algae); % algae status update
            [env, coral, algae] = AlgaeSettlement(env, coral, algae); % algae settlement
        end

        %% Fitness acidification procedure
        if (env.oceanAcidification == true) && (env.currentIter ~= 1) % if acidification is ON...
            [env, coral] = Acidification(env, coral); % decreasing in the coral fitness values
        else
            % copy fitness to struct in actual iteration (they will be taken as reference in Larvae Settings)
            coral.fitness(env.currentIter, :) = coral.fitness(env.currentIter-1, :); 
        end

        %% Larvae settlement procedure
        [env, larvae] = GenLarvae(env, coral); % generation of new larvae
        larvae = Budding(env, coral, larvae); % fragmentation of corals (new larvae)
        [env, coral] = LarvaeSettlement(env, coral, larvae); % larvae settlement
        [env, coral] = NumberOfDetectedSites(env, coral);

        [coral.bestFitness(currentIter), bestIdx] = min(coral.fitness(currentIter, :)); % get min fitness value and index of best coral
        coral.bestCoral(currentIter,:) = coral.population(bestIdx,:); % update best coral history


        %% Summary of solutions display
        if (env.consoleTrace == true) % if console trace is activate, plot results in console
            app.progressTrace.Value = [app.progressTrace.Value; "Iteration: " + env.currentIter];
            plotMaxFit = min(coral.fitness(env.currentIter, :));
            app.progressTrace.Value = [app.progressTrace.Value; "Best Fitness: " + plotMaxFit];
            plotMeanFit(env.currentIter) = mean(coral.fitness(env.currentIter, coral.idx));
            app.progressTrace.Value = [app.progressTrace.Value; "Average Fitness: " + plotMeanFit(env.currentIter)];
            app.progressTrace.Value = [app.progressTrace.Value; "Number of algae:  " + sum(algae.mask)];
            app.progressTrace.Value = [app.progressTrace.Value; "--------------------------------"];
        end

        %% Population update procedure
    %     [env, coral] = UpdateParamPSO(env, coral)
        coral = CoralUpdate(env, coral, algae); % release of new coral 
        env = SubsUpdate(env, coral); % release of new substrate layers
    end
end

