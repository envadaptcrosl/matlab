function [env, coral] = initialization(app)


%%% Initialization: problem structures and properties
    %%%-------------------------------------------------
    addpath("../init"); addpath("../");
    addpath("../subs/mutation"); addpath("../subs/swarm");
    addpath("../subs/physical"); addpath("../subs/crossover");
    addpath("../subs"); addpath("../fitness_function");
    
    env = struct; % definition of structure
    env = GenEnv(env); % definition of 'env' structure

    coral = GenCoral(env); % definition of 'coral' structure

    env.xBest = coral.population;
    env.xBestFit = coral.fitness;
    % copy the best coral results in the 'env' struct / swarm substrates references
    env.gBest(1, :) = coral.bestCoral(1, :);
    env.gBestFit(1, :) = coral.bestFitness(1);

    % update xBest and gBest values
    env = UpdateParamPSO(env, coral);
end