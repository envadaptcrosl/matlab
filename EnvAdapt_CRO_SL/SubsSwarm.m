%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 


function [larvae] = SubsSwarm(env, larvae, population)
    %%% env.subsSwarm = ["PSO", "WOA"]; % swarm substrates
    idxCoral = find( sum( isnan(population), 2 ) == 0 ); % get index of coral positions in the reef
    population = population(idxCoral, :); % remove non coral positions (free or algae sites)
    if (sum(env.probSubsSwarm) > 0) && (sum(env.probSubsTypeSwarm) > 0)
        internalAssign = randsrc(size(population, 1), 1, [env.subsSwarmIdx.'; round((env.probSubsSwarm./env.probSubsTypeSwarm),9).']);
    
        %% Calculate new larvae from an specific substrate
        for k = 1:1:length(env.subsSwarmIdx)
            xBest = env.xBest(env.idxSwarm, :);
            vel = env.psoVel(env.idxSwarm, :);
            fun = str2func( env.subsTable.subs( env.subsSwarmIdx(k) ) ); % set function name
            [population( find(internalAssign == env.subsSwarmIdx(k)), : )] = ...
                fun( env, population( find( internalAssign == env.subsSwarmIdx(k) ), :),...
                xBest( find( internalAssign == env.subsSwarmIdx(k) ), : ),...
                vel( find( internalAssign == env.subsSwarmIdx(k) ), : )); % movement equation
            assignment( find(internalAssign == env.subsSwarmIdx(k) ) ) = env.subsSwarmIdx(k);
        end
        larvae.population = [larvae.population; population];
        larvae.assignment = [larvae.assignment assignment];
    end
end
