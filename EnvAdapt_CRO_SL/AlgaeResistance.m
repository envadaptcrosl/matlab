%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

function [resistance] = AlgaeResistance(algae, rootPositions)
    
    resistance = zeros(1, numel(rootPositions));   % resistance initialization
    
    for k = 1:numel(rootPositions)    % Position iteration
        % boundary [b, c] centered in position k
        boundInit = max(1, rootPositions(k) - algae.b);
        boundEnd = min(numel(algae.mask), rootPositions(k) + algae.c);
       
        resistance(k) = algae.maxR - sum(algae.mask(boundInit:boundEnd)); % Sum of algae within the boundary
    end

end