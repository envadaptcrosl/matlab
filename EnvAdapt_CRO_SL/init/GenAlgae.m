%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 


function algae = GenAlgae(env)
    algae.RDiscount = ones(env.reefSize, 1); % Life discount 
    algae.R = NaN * zeros(env.reefSize, 1); % algae lifes
    algae.mask = zeros(env.reefSize, 1); % mask of occupied positions by algae (0 free, 1 occupied)
    algae.harmFactor = 1; % value of life decreased per iteration
    
    algae.b = 3;
    algae.c = 3;
    algae.maxR = algae.b + algae.c; % max value of life for each alga
    algae.rootingProb = 0.8; % rooting probability of an alga in an empty position
    algae.maxNumber = 0.8 * env.reefSize; % max number of algae rooted in the reef
    algae.sigma = round(0.05*env.reefSize);
    algae.posRecord(1, :) = algae.mask;
end