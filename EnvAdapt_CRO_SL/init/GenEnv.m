%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 


%% Default mode: Minimization problem
function [env] = GenEnv(env)

    %% Other initial settings (optional)
    
    % Section for other additional settings needed to execute the process
    % env.seed = 12341243; # MinMax Seed Range = [0..2^32)
    % env.prng="twister";
    %%%
    %% Environment properties
    %%%----------------------
    env.objFunction = @objFunction_spring1;
    env.nIterations = 500; % established stop criteria for a determinated number of iterations
    env.currentIter = 1; % start current iteration at 1
    env.varType = "float"; % type of numerical values for the env: "int" or "float"
    env.varBdry = [ 0.05, 0.25, 2 ; 2, 1.30, 15 ]; % Max and min boundaries
    env.repeatedVals = true; % boolean value if the population can repeat values
    env.reefSize = 75; % number of available positions in the reef
    env.reefFilled = 25; % reefFilled <= reefSize
    env.filledPos = randperm(env.reefSize, env.reefFilled);
    env.consoleTrace = true; % display max and avg fitness results and substrates participation
    env.currentIter = 1;
    env.nVars = 3;
    env.fitnessCalls(1) = 0;
    env.fitnessCallsEff(1) = 0;
    env.trend = "min"; % trend of optimization process: "max" (default) or "min"
    if (env.trend == "max") % Problem: maximize the objective/fitness function
        env.emptyPosVal = -Inf; % max values for empty position to be replaceable easily
    elseif (env.trend == "min") % Problem: minimize the objective/fitness function
        env.emptyPosVal = Inf; % max values for empty position to be replaceable easily
    else
        error("ERROR: optimization trend 'env.trend' not supported."); % Not trend supported
    end
    
    

    %% Substrates properties
    %%%---------------------
    env.subsX = ["OnePx","TwoPx", "BLX"]; % crossover substrates // OnePx, TwoPx, MPx, BLX
    env.subsMut = ["RandM", "GM"]; % mutation substrates // RandM, GM, FlipM
    env.subsSwarm = ["PSO"]; % swarm substrates // PSO
    env.subsPhys = []; % physical-based substrates // SA
    env.subsHum = []; % human-based substrates // TS
    %%% Number of Substrates per Substrate group
    env.nSubsX = length(env.subsX); % number of cross substrates
    env.nSubsMut = length(env.subsMut); % number of mutation substrates
    env.nSubsSwarm = length(env.subsSwarm); % number of swarm substrates
    env.nSubsPhys = length(env.subsPhys); % number of physical-based substrates    
    env.nSubsHum = length(env.subsHum); % number of human-based substrates
    %%% Assembly of the substrate layers structure
    env.subsType = repelem(["Crossover", "Mutation", "Swarm", "Physical", "Human"],...
        [env.nSubsX, env.nSubsMut, env.nSubsSwarm, env.nSubsPhys, env.nSubsHum]);
    env.subs = cat(2, env.subsX, env.subsMut, env.subsSwarm, env.subsPhys, env.subsHum);
    env.nSubs = length(env.subs); % substrates size
    env.subsRank = ones(env.nSubs,1); % number of participation along the iterations (started at 0)
    env.subsBest = zeros(env.nSubs,1);
    env.existSubs = [~isempty(env.subsX) ~isempty(env.subsMut) ~isempty(env.subsSwarm) ~isempty(env.subsPhys) ~isempty(env.subsHum)];
    
    env.subsTypeProb = env.existSubs./sum( env.existSubs );
    
    
    env.probSubsTypeX = env.subsTypeProb(1);  % cross substrates type probability initialization
    env.probSubsTypeMut = env.subsTypeProb(2);  % mutation substrates type probability initialization
    env.probSubsTypeSwarm = env.subsTypeProb(3);  % swarm substrates type probability initialization
    env.probSubsTypePhys = env.subsTypeProb(4); % physical-based substrates type probability initialization
    env.probSubsTypeHum = env.subsTypeProb(5); % human-based substrates type probability initialization

    %%% global probability for each cross substrate:
    env.probSubsX = env.probSubsTypeX.*ones(length(env.subsX),1)./env.nSubsX;
    %%% global probability for each mutation substrate:
    env.probSubsMut = env.probSubsTypeMut.*ones(length(env.subsMut),1)./env.nSubsMut;
    %%% global probability for each Swarm substrate:
    env.probSubsSwarm = env.probSubsTypeSwarm.*ones(length(env.subsSwarm),1)./env.nSubsSwarm;
    %%% global probability for each physical-based substrate
    env.probSubsPhys = env.probSubsTypePhys.*ones(length(env.subsPhys),1)./env.nSubsPhys;
    %%% global probability for each human-based substrate
    env.probSubsHum = env.probSubsTypeHum.*ones(length(env.subsHum),1)./env.nSubsHum;
    env.subsProb = cat(1, env.probSubsX, env.probSubsMut, env.probSubsSwarm, env.probSubsPhys, env.probSubsHum); % array of all subs probabilities
    
    
    %% Table of substrates info
    %%%------------------------
    subs = env.subs.';
    env.subsTable = table(subs);
    env.subsTable.subsType = env.subsType.';
    env.subsTable.subsRank = env.subsRank;
    env.subsTable.subsBest = env.subsBest;
    env.subsTable.subsProb = env.subsProb;
    %env.subsTable = convertvars(env.subsTable,{'subs','subsType'},'categorical');
    if(~isempty(env.subsX))
        env.subsXIdx = find( ismember(env.subsTable.subs, env.subsX) ); % index of cross substrates in the table
    end
    if(~isempty(env.subsMut))
        env.subsMutIdx = find( ismember(env.subsTable.subs, env.subsMut) ); % index of mutation substrates in the table
    end
    if(~isempty(env.subsSwarm))
        env.subsSwarmIdx = find( ismember(env.subsTable.subs, env.subsSwarm) ); % index of swarm substrates in the table
    end
    if(~isempty(env.subsPhys))
        env.subsPhysIdx = find( ismember(env.subsTable.subs, env.subsPhys) ); % index of physical-based substrates in the table
    end
    if(~isempty(env.subsHum))
        env.subsHumIdx = find( ismember(env.subsTable.subs, env.subsHum) ); % index of human-based substrates in the table
    end
    % assignment of reef positions to a determined substrate
%     subs = 1:1:length(env.subsProb);
%     env.subsAssign = randsrc(env.reefSize, 1, [subs; env.subsProb.']);
    
    env.subsTypeProb(find( env.subsTypeProb == 0 )) = [];
    subsTypes = 1:1:length(env.subsTypeProb);
    env.subsAssign = randsrc(env.reefSize, 1, [subsTypes; env.subsTypeProb]);
    %% Environmental Adaptive operators
    %%%--------------------------------
    env.oceanAlgae = true;
    env.oceanAcidification = true; % activation of acidification process
    if (env.oceanAcidification == true)
        if(env.oceanAlgae == true)
            env.H0 = 3.25*10^-8; % maximum H+ value
            env.Hmin = 6.31*10^-9; % mininum H+ value
            env.pHmax = -log10(env.Hmin);
            env.incrH = abs(env.H0 - env.Hmin); % range of values for H
            env.lambda = 1; % adjust factor for acidification calculation
        else
            env.acidBdry = [0.998 1]; % boundary of fitness reduction factor per iteration
        end
    end
    

    %% substrates properties

    % Crossover and Mutation substrates

    env = GenCrossoverMutation(env);
    
    % Swarm-based substrates
    
    if( ~isempty(env.subsSwarm) )
       env = GenSwarm(env); 
    end

    % Physical-based substrates
    
    if( ~isempty(env.subsPhys) )
       env = GenPhysical(env); 
    end

    % Human-based substrates

    if( ~isempty(env.subsHum) )
       env = GenHuman(env); 
    end
    
end
