%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 


function [coral] = GenCoral(env)
    %% Coral general properties
    coral.population = NaN.*zeros(env.reefSize, env.nVars); % initial definition of coral population matrix
    if (env.repeatedVals == true) % repeated values allowed
        coral.population(1:1:env.reefFilled, :) = env.varBdry(1, :) + (env.varBdry(2) - env.varBdry(1)).* ...
            rand(env.reefFilled, env.nVars); % random population
    elseif (env.repeatedVals == false) && (env.varType == "int")
        for k = 1:1:length(env.reefFilled) % non repeated values allowed
            if env.varType == "float"
                coral.population(1:1:env.reefFilled, :) = env.varBdry(1, :) + (env.varBdry(2) - env.varBdry(1)).* ...
                rand(env.reefFilled, env.nVars); % random population
                coral.idx = find( sum(isnan(coral.population), 2) == 0 );
                coral = NonRepeatedVals(env, coral);
            end
        end
    else
        for k = 1:1:length(env.reefFilled) % non repeated values allowed
            if env.varType == "float"
                 % random population
                coral.population(1:1:env.reefFilled, :) = env.varBdry(1, :) +...
                    (env.varBdry(2) - env.varBdry(1)).* rand(env.reefFilled, env.nVars);
                coral = NonRepeatedVals(env, coral);
            end
        end
    end
    

    %% For standard version of CRO, not applied to CRO-SL case
    % coral.Fb = 0.99; % external sexual reproduction (F_b: broadcast spawning)
    % %%% Brooding spawning --> f_b = 1 - F_b
    % coral.fb = 1 - coral.Fb % Internal sexual reproduction (f_b: brooding spawning)
    
    %% Coral death rate, budding and fitness properties
    % Note: coral.Fa + coral.deathRate <= 1
    coral.deathRate = 0.3; % death probabilities in each iteration per individual
    coral.Fa = 0.01; % Budding max probabilities

    coral.fitness = env.emptyPosVal.*ones(1, env.reefSize); % initial definition of the fitness array    
    coral.idx = find( sum(isnan(coral.population), 2) == 0 );
    coral.fitness(1, coral.idx) = EvaluatePopulation(env, coral.population(coral.idx, :) );
    if env.trend == "max"
        [coral.bestFitness(1), bestIdx] = max(coral.fitness(1, :)); % get max fitness value and index of best coral
    else
        [coral.bestFitness(1), bestIdx] = min(coral.fitness(1, :)); % get min fitness value and index of best coral
    end
    coral.bestCoral(1, :) = coral.population(bestIdx, :); % update best coral history
   
end
