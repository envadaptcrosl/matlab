%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 


function env = GenCrossoverMutation(env)
    % Points Crossover
    env.nXPoints = 1; % number of cross points (this value changes when the substrate is selected)
    env.mutationLarvaProb = 0.15; % mutation probability of each larva
    env.mutationVarProb = 0.5; % mutation probability of each variable
    
    % Blend Crossover
    env.eta = 2;
    env.BLXa = 0.1;
    env.uDist = 1:env.nIterations; % uniform distribution
    env.BLXb = [2.*env.uDist.*(1/(env.eta + 1)) 2.*1-(env.uDist(1:50)).*(1/(env.eta + 1))];
    
    % Gaussian Mutation

    env.sigma = 0.001;

    % 

end