%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

function population = BoundConstraint(env, population)
    lowVarBdry = repmat( env.varBdry(1, :), size(population, 1), 1 ); % variable lower boundaries matrix
    upperVarBdry = repmat( env.varBdry(2, :), size(population, 1), 1 ); % variable upper boundaries matrix
    
    [idxRow, idxCol] = find(population <= lowVarBdry);
    if ~( isempty(idxRow) || isempty(idxCol) ) % if lower boundaries have been surpassed
        for k = 1:1:length(idxRow)

            population(idxRow(k), idxCol(k)) = env.varBdry(1, idxCol(k)); % constraint of the values to the minimum

        end
    end
    [idxRow, idxCol] = find(population >= upperVarBdry);
    if ~( isempty(idxRow) || isempty(idxCol) ) % if lower boundaries have been surpassed
        for k = 1:1:length(idxRow)

            population(idxRow(k), idxCol(k)) = env.varBdry(2, idxCol(k)); % constraint of the values to the minimum

        end
    end
    
end