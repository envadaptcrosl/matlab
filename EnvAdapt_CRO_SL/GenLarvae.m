%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Title: EnvAdapt-CRO-SL algorithm
%   Main author: AJ Romero-Barrera
%   Contributors: L Cruz-Piris, M Tejedor-Romero, JM Gimenez-Guzman, I Marsa-Maestre
%   Date: 2024
%   Code version: 1.0
%   Availability: https://gitlab.com/envadaptcrosl/envadaptcrosl-matlab
%   SPDX-License-Identifier: GPL-3.0-only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 


function [env, larvae] = GenLarvae(env, coral)

    %% Get index of corals subjected to each substrate
    env.idxCross = find(env.subsAssign == 1); % get indexes of Coral population subjected to cross Substrates
    env.idxMutation = find(env.subsAssign == 2); % get indexes of Coral population subjected to mutation Substrates
    env.idxSwarm = find(env.subsAssign == 3); % get indexes of Coral population subjected to swarm Substrates
    env.idxPhysical = find(env.subsAssign == 4); % get indexes of Coral population subjected to physical Substrates
%     larvae = struct;
    
        
    idxNoNaN = find( ~isnan(coral.population(:, 1)) );
    population = coral.population(idxNoNaN, :);
    
    larvae.population = [];
    larvae.assignment = [];
    %% Generation of larvae population
    if (~isempty(env.subsX))
        larvae = SubsCross( env, larvae, coral.population(env.idxCross,:) ); % generate larvae from cross substrates
    end
    if (~isempty(env.subsMut))
        larvae = SubsMutation( env, larvae, coral.population(env.idxMutation,:) ); % generate larvae from mutation substrates
    end
    if (~isempty(env.subsSwarm))
        larvae = SubsSwarm( env, larvae, coral.population(env.idxSwarm,:) ); % generate swarm from swarm substrates
    end
    if (~isempty(env.subsPhys))
        larvae = SubsPhysical( env, larvae, coral.population(env.idxPhysical,:) ); % generate larvae from physical substrates
    end
    %% Evaluate larvae fitness
    larvae.population = BoundConstraint(env, larvae.population( find(~isnan( larvae.population(:, 1) ) ), : ) );
    larvae.szPopulation = size(larvae.population, 1);
    larvae.fitness = EvaluatePopulation(env, larvae.population);
    larvae.setAttemps = randi(4, size(larvae.population, 1), 1);
    larvae.rootProb = 0.95;


    env.fitnessCalls(env.currentIter) = length(larvae.fitness);
end