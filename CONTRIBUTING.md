## Main contributors

- **Antonio J. Romero Barrera** | [Scholar](https://scholar.google.es/citations?user=2HE_0PEAAAAJ) | [LinkedIn](https://www.linkedin.com/in/antonio-jose-romero-barrera/)
- **Luis de la Cruz Piris** | [Scholar](https://scholar.google.es/citations?user=c2r5WIIAAAAJ) | [LinkedIn](https://www.linkedin.com/in/luiscruzpiris/)
- **Marino Tejedor Romero**
- **José Manuel Giménez Guzmán** | [Scholar](https://scholar.google.es/citations?user=oZy4KjsAAAAJ)
- **Iván Marsá Maestre** | [Scholar](https://scholar.google.es/citations?user=OV714UgAAAAJ) | [LinkedIn](https://www.linkedin.com/in/iv%C3%A1n-mars%C3%A1-maestre-5a12192b/)
